import { IUsuario } from "../../../entities/usuario/usuario.interface";


export const usuarios: IUsuario[] = [
    {
        IdUsuario: 1,
        Alias: 'francisco',
        Correo: 'francisco.rdzz@gmail.com',
        Contrasena: '12345',
        ImagenPerfil: null,
        FechaBajaLogica: null,
        FechaUltimaMod: new Date(),
        BajaLogica: false
    }
]