
import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Usuario } from '../../../entities/usuario/usuario.entity';
import { usuarios } from '../Usuario/usuario.seed'
import { IUsuario } from '../../../entities/usuario/usuario.interface';

@Injectable()
export class UsuarioSeederService {
    constructor(
        // private readonly logger: Logger,
        @InjectRepository(Usuario)
        private readonly usuarioRepository: Repository<Usuario>,
    ) { }

    checkForExistingData(): Promise<Usuario> {
        return this.usuarioRepository.findOne();
    }

    create(): Array<Promise<Usuario>> {
        return usuarios.map(async (usuario: IUsuario) => {
            return await this.usuarioRepository
                .findOne({ IdUsuario: usuario.IdUsuario })
                .then(async dbUsuario => {
                    if (dbUsuario) {
                        return Promise.resolve(null);
                    }
                    return Promise.resolve(
                        this.usuarioRepository.insert(usuario),
                    );
                })
                .catch(error => Promise.reject(error));
        });
    }
}
