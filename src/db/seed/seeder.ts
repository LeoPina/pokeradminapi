import { Injectable } from "@nestjs/common";
import { UsuarioSeederService } from "./Usuario/usuario-seed.service";

@Injectable()
export class Seeder {
    constructor(
        private readonly usuariosSeedService: UsuarioSeederService
    ) { }
    async seed() {
        await this.usuarios()
            .then(completed => {
                Promise.resolve(completed);
            })
            .catch(error => {
                Promise.reject(error);
            });
    }


    async usuarios() {
        const usuario = await this.usuariosSeedService.checkForExistingData();

        if (!usuario) {
            return await Promise.all(this.usuariosSeedService.create())
                .then(createdLanguages => {
                    // this.logger.debug(
                    //     'No. of estados created : ' +
                    //     createdLanguages.filter(nullValueOrCreated => nullValueOrCreated)
                    //         .length
                    // );
                    return Promise.resolve(true);
                })
                .catch(error => Promise.reject(error));
        }

        // this.logger.debug('Estados already seeded!');
    }
}
