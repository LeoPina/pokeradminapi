import { Module, Logger } from '@nestjs/common';
import { Usuario } from '../../entities/usuario/usuario.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsuarioSeederService } from './Usuario/usuario-seed.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            Usuario
        ])
    ],
    providers: [
        UsuarioSeederService,
        Logger
    ],
    exports: [
        UsuarioSeederService
    ]
})
export class SeedModule {}
