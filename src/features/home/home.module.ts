import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Corte } from '../../entities/corte/corte.entity';
import { HomeController } from './home.controller';
import { CorteService } from '../../entities/corte/corte.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            Corte
          ])
    ],
    controllers: [HomeController],
    providers: [CorteService]
})
export class CorteModule {}
