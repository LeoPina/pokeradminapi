import { ApiProperty } from "@nestjs/swagger";

export class UsuarioLoginDto{
    @ApiProperty()
    readonly correo: string;
    @ApiProperty()
    readonly contrasena: string;
}