export interface JwtPayload {
    correo: string;
    iat?: Date;
}