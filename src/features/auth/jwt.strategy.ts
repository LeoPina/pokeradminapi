import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy, VerifiedCallback } from 'passport-jwt';

import { UsuarioService } from '../../entities/usuario/usuario.service';
import { SECRET } from '../../shared/config/constants';
import { JwtPayload } from './jwt-payload.model';
import { UserToken } from './user-token';


@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly usuarioService: UsuarioService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: true,
      secretOrKey: SECRET,
    });
  }

  async validate(payload: JwtPayload, done: VerifiedCallback) {

    const user = await this.usuarioService.findUserByEmail(payload.correo);

    if (!user) {
      return done(new HttpException({}, HttpStatus.UNAUTHORIZED), false);
    }

    const userToken: UserToken = {
      idUsuario: user.IdUsuario,
      correo: user.Correo
    };

    return done(null, userToken, payload.iat);
  }
}
