import { Injectable, OnModuleInit } from '@nestjs/common';
import { UsuarioService } from '../../entities/usuario/usuario.service';
import { ModuleRef } from '@nestjs/core';
import { UsuarioLoginDto } from './usuario-login.dto';
import { Usuario } from '../../entities/usuario/usuario.entity';
import { BusinessException } from '../../shared/exceptions/business.exception';
import { JwtService } from '@nestjs/jwt';
import { UsuarioCreacionDto } from './usuario-creacion.dto';

@Injectable()
export class AuthService implements OnModuleInit {
    private usuarioService: UsuarioService;
    private jwtService: JwtService;

    constructor( private readonly moduleRef: ModuleRef){}

    onModuleInit(){
        this.usuarioService = this.moduleRef.get(UsuarioService, { strict: false });
        this.jwtService = this.moduleRef.get(JwtService, { strict: false });
    }

    async validateUser(usuarioLoginDto: UsuarioLoginDto): Promise<Usuario>{
        const user = this.usuarioService.findUserByEmail(usuarioLoginDto.correo);
        if(!user)
            throw new BusinessException('Usuario no encontrado.');
        
        return user;
    }

    async createUser(usuarioCreacionDto: UsuarioCreacionDto): Promise<void>{
        const user = await this.usuarioService.findUserByEmailAllowNull(usuarioCreacionDto.correo);

        if(!user){
            return await this.usuarioService.createUser(usuarioCreacionDto.alias, usuarioCreacionDto.correo, usuarioCreacionDto.contrasena, usuarioCreacionDto.imagenPerfil);
        }

        throw new BusinessException('Este correo ya se encuentra registrado.');
    }

    async login(userLogin: UsuarioLoginDto) {
        const user = await this.usuarioService.findUserByEmail(userLogin.correo);
    
        const payload = {
          correo: user.Correo
        };
    
        const accessToken = this.jwtService.sign(payload);
    
        return {
          access_token: accessToken,
          expires_in: 3600,
          status: 200,
        };
      }
}
