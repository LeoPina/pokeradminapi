import { Module } from '@nestjs/common';
import { Usuario } from '../../entities/usuario/usuario.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsuarioService } from '../../entities/usuario/usuario.service';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { SECRET } from '../../shared/config';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { LocalStrategy } from './local.strategy';
import { JwtStrategy } from './jwt.strategy';



@Module({
    imports: [
        TypeOrmModule.forFeature([
            Usuario
          ]),
          PassportModule.register({
            defaultStrategy: 'jwt',
            property: 'user',
            session: false,
        }),
        JwtModule.register({
            secret: SECRET,
            signOptions: { expiresIn: '1d' },
        }),
    ],
    providers: [
        UsuarioService, 
        AuthService,
        LocalStrategy,
        JwtStrategy,
    ],
    controllers: [AuthController]
})
export class UsuarioModule {}
