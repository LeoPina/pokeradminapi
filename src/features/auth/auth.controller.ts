import { Controller, Post, Body, UseGuards } from '@nestjs/common';
import { UsuarioLoginDto } from './usuario-login.dto';
import { AuthService } from './auth.service';
import { ApiBadRequestResponse, ApiBearerAuth, ApiOkResponse, ApiOperation } from '@nestjs/swagger';
import { UsuarioCreacionDto } from './usuario-creacion.dto';
import { AuthGuard } from '@nestjs/passport';
import { ApiModelException } from '../../shared/errors/api-model.exceptions';


@Controller('auth')
export class AuthController {
    constructor(
        private readonly authService: AuthService
    ){}
    
    @ApiOperation({description: 'Realiza el registro y creación de usuario.'})
    @Post('registro')
    async registro(@Body() user: UsuarioCreacionDto): Promise<any>{
        return this.authService.createUser(user);
    }

    @ApiOperation({ description: 'Realiza el inicio de sesión.' })
    @UseGuards(AuthGuard('local'))
    @ApiOkResponse({ description: 'Ok', type: UsuarioLoginDto })
    @ApiBadRequestResponse({ description: 'BadRequest', type: ApiModelException })
    @Post('login')
    async login(@Body() user: UsuarioLoginDto): Promise<any>{
        return this.authService.validateUser(user);
    }

}
