import { ApiProperty } from "@nestjs/swagger";

export class UsuarioCreacionDto{
    @ApiProperty()
    readonly alias: string;
    @ApiProperty()
    readonly correo: string;
    @ApiProperty()
    readonly contrasena: string;
    @ApiProperty()
    readonly imagenPerfil: Buffer;
}