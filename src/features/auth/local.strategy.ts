import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';

import { AuthService } from './auth.service';
import { UsuarioLoginDto } from './usuario-login.dto';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly authService: AuthService) {
    super({
      usernameField: 'correo',
      passwordField: 'contrasena',
    });
  }

  async validate(email: string, password: string): Promise<any> {

    const userVerification: UsuarioLoginDto = {
      correo: email,
      contrasena: password
    }

    const user = await this.authService.validateUser(userVerification);

    if (!user) {
      throw new UnauthorizedException();
    }

    return user;
  }
}
