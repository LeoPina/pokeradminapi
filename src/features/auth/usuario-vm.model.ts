import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class UsuarioViewModel{
    @Expose()
    readonly Alias: string;
    @Expose()
    readonly Correo: string;
    @Expose()
    readonly ImagenPerfil: Buffer;
}