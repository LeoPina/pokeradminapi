import { Module } from '@nestjs/common';
import { Mesa } from '../../entities/mesa/mesa.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdministrarController } from './administrar.controller';
import { MesaService } from '../../entities/mesa/mesa.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            Mesa
          ])
    ],
    controllers: [AdministrarController],
    providers: [MesaService]
})
export class AdministrarModule {}
