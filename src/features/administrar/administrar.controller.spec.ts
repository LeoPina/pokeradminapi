import { Test, TestingModule } from '@nestjs/testing';
import { AdministrarController } from './administrar.controller';

describe('Administrar Controller', () => {
  let controller: AdministrarController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AdministrarController],
    }).compile();

    controller = module.get<AdministrarController>(AdministrarController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
