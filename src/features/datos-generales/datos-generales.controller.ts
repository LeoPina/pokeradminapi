import { Controller, Body, Get } from '@nestjs/common';
import { DatosGeneralesService } from './datos-generales.service';
import { ApiBadRequestResponse, ApiBearerAuth, ApiNoContentResponse, ApiOperation} from '@nestjs/swagger';

// @ApiBearerAuth()
// @UseGuards(AuthGuard('jwt'))
@Controller('datos-generales')
export class DatosGeneralesController {
  constructor(private readonly datosGeneralesService: DatosGeneralesService) {}

  @ApiOperation({ description: 'Busca datos generales de usuario por corte' })
  @ApiNoContentResponse({ description: 'No Content' })
  // @ApiBadRequestResponse({ description: 'Bad Request', type: ApiModelException })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  @Get('buscaMesaPorCorte')
  async buscaMesaPorCorte(@Body() corte: number): Promise<void> {
      await this.datosGeneralesService.buscaMesaPorCorte(corte);
  }

  @ApiOperation({ description: 'Busca saldos de un usuario' })
  @ApiNoContentResponse({ description: 'No Content' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  @Get('buscaSaldosPorUsuario')
  async buscaSaldosPorUsuario(@Body() userId: number): Promise<void> {
      await this.datosGeneralesService.buscaSaldosPorUsuario(userId);
  }
}
