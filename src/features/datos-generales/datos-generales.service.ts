import { Injectable } from '@nestjs/common';
import { ModuleRef } from '@nestjs/core';
import { MesaService } from '../../entities/mesa/mesa.service';
import { HistorialSaldoService } from '../../entities/historial-saldo/historial-saldo.service';

@Injectable()
export class DatosGeneralesService {
  private mesaService: MesaService;
  private historialSaldoService: HistorialSaldoService;

  constructor(private readonly moduleRef: ModuleRef) {}
  onModuleInit() {
    this.mesaService = this.moduleRef.get(MesaService, { strict: false });
    this.historialSaldoService = this.moduleRef.get(HistorialSaldoService, {
      strict: false,
    });
  }

  async buscaMesaPorCorte(corteId: number): Promise<void> {
    await this.mesaService.findByCorte(corteId);
  }

  async buscaSaldosPorUsuario(userId: number): Promise<void> {
    await this.historialSaldoService.findByUser(userId);
  }

  async mostrarUltimoCorte(): Promise<void> {
    await this.historialSaldoService.findByFechaCorte();
  }
}
