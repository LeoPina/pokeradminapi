import { Module } from '@nestjs/common';
import { HistorialSaldo } from '../../entities/historial-saldo/historial-saldo.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DatosGeneralesController } from './datos-generales.controller';
import { HistorialSaldoService } from '../../entities/historial-saldo/historial-saldo.service';
import { DatosGeneralesService } from './datos-generales.service';

@Module({
  imports: [TypeOrmModule.forFeature([HistorialSaldo])],
  controllers: [DatosGeneralesController],
  providers: [HistorialSaldoService, DatosGeneralesService],
})
export class DatosGeneralesModule {}
