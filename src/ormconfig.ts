import { ConnectionOptions } from 'typeorm';
import * as dotenv from 'dotenv';
import * as fs from 'fs';

const environment = process.env.NODE_ENV || 'development';
const data: any = dotenv.parse(fs.readFileSync(`.env.${environment}`));

console.log(data);

const config: ConnectionOptions = {
  type: 'mssql',
  host: data.DATABASE_HOST,
  username: data.DATABASE_USER,
  password: data.DATABASE_PASSWORD,
  database: data.DATABASE_NAME,
  schema: data.DATABASE_SCHEMA,
  entities: [__dirname + '/**/**/*.entity{.ts,.js}'],
  synchronize: false,
  logging: false,
  migrations: [__dirname + '/db/migrations/**/*{.ts,.js}'],
  cli: {
    migrationsDir: 'src/db/migrations'
  },
  options: {
    encrypt: true,
    packetSize: 81920
  },
  requestTimeout: 300000
};

export = config;
