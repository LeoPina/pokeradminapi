export interface IUsuario {
    IdUsuario: number;
    Alias: string;
    Correo: string;
    Contrasena: string;
    ImagenPerfil: Buffer;

    FechaUltimaMod: Date;
    BajaLogica: boolean;
    FechaBajaLogica: Date;
}
