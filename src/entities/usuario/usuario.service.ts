import { InjectRepository } from '@nestjs/typeorm';
import { Usuario } from './usuario.entity';
import { Repository } from 'typeorm';
import { UsuarioCorreoDto } from '../../features/auth/usuario-correo.dto';
import { UsuarioViewModel } from '../../features/auth/usuario-vm.model';
import { UsuarioLoginDto } from '../../features/auth/usuario-login.dto';
import { plainToClass } from 'class-transformer';
import { Injectable } from '@nestjs/common';

@Injectable()
export class UsuarioService {
    constructor(
        @InjectRepository(Usuario)
        private readonly usuarioRepository: Repository<Usuario>
    ){}

    async findAll(): Promise<Usuario[]>{
        const usuarios = await this.usuarioRepository.find();
        if(!usuarios)
            throw 'No se encontró ningún usuario.';
        return usuarios;
    }

    async findUserByEmail(correo: string): Promise<Usuario>{
        const usuario = await this.usuarioRepository.findOne({where: { Correo: correo }});
        if(!usuario)
            throw 'El usuario no existe.';
        return usuario;
    }

    async findUserByEmailAllowNull(correo: string): Promise<Usuario> {
        const usuario = await this.usuarioRepository.findOne({ where: { Correo: correo } });
    
        return usuario;
    }

    async createUser(alias: string, correo: string, contrasena: string, imagenPerfil: Buffer): Promise<void>{
        await this.usuarioRepository.query(`POKSh.CrearNuevoUsuarioProc @Alias = ${alias}, @Correo = ${correo}, @Constrasena = ${contrasena}, @ImagenPerfil = ${imagenPerfil}`);
    }
}
