import { Entity, PrimaryColumn, Column, PrimaryGeneratedColumn, OneToMany, JoinColumn } from 'typeorm'
import { BajaLogica } from '../baja-logica';
import { IUsuario } from './usuario.interface';
import { HistorialSaldo } from '../historial-saldo/historial-saldo.entity';

@Entity({ name: 'Usuario' })
export class Usuario extends BajaLogica implements IUsuario {
    @PrimaryGeneratedColumn()
    IdUsuario: number;

    @Column({ type: 'varchar', nullable: false })
    Alias: string;

    @Column({ type: 'varchar', nullable: false })
    Correo: string;

    @Column({ type: 'varchar', nullable: false })
    Contrasena: string;

    @Column({ type: 'varbinary' })
    ImagenPerfil: Buffer;

    @OneToMany('HistorialSaldo', 'usuarios')
    @JoinColumn([
        {
            name: 'IdUsuario',
            referencedColumnName: 'IdUsuario'
        }
    ])
    saldos: HistorialSaldo[];
}