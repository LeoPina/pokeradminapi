export interface IHistorialSaldo {
    IdHistorialSaldo: number;
    IdUsuario: number;
    IdMesa: number;
    Saldo: number;
}
