import { Entity, PrimaryColumn, Column, ManyToOne, JoinColumn, PrimaryGeneratedColumn } from "typeorm";
import { Mesa } from "../mesa/mesa.entity";
import { Usuario } from "../usuario/usuario.entity";

@Entity({ name: 'HistorialSaldo' })
export class HistorialSaldo {
    @PrimaryGeneratedColumn()
    IdHistorialSaldo: number;

    @Column({ nullable: false })
    IdUsuario: number;

    @Column({ nullable: false })
    IdMesa: number;

    @Column({ nullable: false })
    Saldo: number;

    @ManyToOne('Usuario', 'saldos')
    @JoinColumn([
        {
            name: 'IdUsuario',
            referencedColumnName: 'IdUsuario'
        }
    ])
    usuarios: Usuario;

    @ManyToOne('Mesa', 'saldos')
    @JoinColumn([
        {
            name: 'IdMesa',
            referencedColumnName: 'IdMesa'
        }
    ])
    mesas: Mesa;
}