import { Test, TestingModule } from '@nestjs/testing';
import { HistorialSaldoService } from './historial-saldo.service';

describe('HistorialSaldoService', () => {
  let service: HistorialSaldoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HistorialSaldoService],
    }).compile();

    service = module.get<HistorialSaldoService>(HistorialSaldoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
