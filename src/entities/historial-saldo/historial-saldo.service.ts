import { Injectable } from '@nestjs/common';
import { HistorialSaldo } from './historial-saldo.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, MaxKey, LessThan } from 'typeorm';
import { Mesa } from '../mesa/mesa.entity';
import { Corte } from '../corte/corte.entity';

@Injectable()
export class HistorialSaldoService {
  constructor(
    @InjectRepository(HistorialSaldo)
    private readonly historialSaldoRepository: Repository<HistorialSaldo>,
  ) {}

  // async findAll(): Promise<HistorialSaldo[]> {
  //   const saldos = await this.historialSaldoRepository.find({
  //     relations: ['usuarios', 'mesas'],
  //   });
  //   console.log('####### Saldos findAll', saldos);
  //   if (!saldos) throw 'No se encontró ningúna mesa.';
  //   return saldos;
  // }

  async findByUser(userId: number): Promise<HistorialSaldo[]> {
    const IdUsuario = 1;
    const saldos = await this.historialSaldoRepository.find({
      where: { IdUsuario: IdUsuario },
      relations: ['usuarios', 'mesas'],
    });
    console.log('####### Saldos findByUser', saldos);
    if (!saldos) throw 'No se encontró ningúna mesa.';
    return saldos;
  }

  async findByFechaCorte(): Promise<HistorialSaldo> {
    const IdUsuario = 1;
    const saldo = await this.historialSaldoRepository
      .createQueryBuilder('hs')
      .leftJoin(Mesa, 'm', 'm."IdMesa" = hs.IdMesa')
      .leftJoin(Corte, 'c', 'c."IdCorte" = m.IdCorte')
      .where('hs."IdUsuario" = :IdUsuario', { IdUsuario: IdUsuario })
      .orderBy('c.FechaInicial', 'DESC')
      .getOne();
    console.log('####### Saldo findByFechaCorte', saldo);
    if (!saldo) throw 'No se encontró ningúna mesa.';
    return saldo;
  }
}
