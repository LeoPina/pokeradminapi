import { Entity, Column, PrimaryGeneratedColumn, OneToMany, JoinColumn } from "typeorm";
import { Mesa } from "../mesa/mesa.entity";

@Entity({ name: 'Corte' })
export class Corte {
    @PrimaryGeneratedColumn()
    IdCorte: number;

    @Column({ nullable: false })
    FechaInicial: Date;

    @Column({ nullable: false })
    FechaFinal: Date;

    @Column({ type: 'varchar', nullable: false })
    NombreCorte: string;

    @OneToMany('Mesa', 'cortes')
    @JoinColumn([
        {
            name: 'IdCorte',
            referencedColumnName: 'IdCorte'
        }
    ])
    mesas: Mesa[];
}