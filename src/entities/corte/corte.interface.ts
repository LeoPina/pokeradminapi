export interface IUsuario {
    IdCorte: number;
    FechaInicial: Date;
    FechaFinal: Date;
    NombreCorte: string;
}
