import { Column } from 'typeorm';

export abstract class BajaLogica {
    @Column({ nullable: false, type: 'datetime' })
    FechaUltimaMod: Date;

    @Column({ default: false })
    BajaLogica: boolean;

    @Column({ nullable: true })
    FechaBajaLogica: Date;
}