export interface IMesa {
    IdMesa: number;
    IdCorte: number;
    Fecha: Date;
    ClaEstatus: number;
    CreditoInicial: number;
}
