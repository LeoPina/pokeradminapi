import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Mesa } from './mesa.entity';
import { Repository } from 'typeorm';

@Injectable()
export class MesaService {
  constructor(
    @InjectRepository(Mesa)
    private readonly mesaRepository: Repository<Mesa>,
  ) {}

  // async findAll(): Promise<Mesa[]> {
  //   const mesas = await this.mesaRepository.find();
  //   console.log('####### Mesas findAll', mesas);
  //   if (!mesas) throw 'No se encontró ningúna mesa.';
  //   return mesas;
  // }

  async findByCorte(idCorte: number): Promise<Mesa[]> {
    const mesas = await this.mesaRepository.find({
      where: { IdCorte: idCorte },
      relations: ['cortes'],
    });
    console.log('####### Mesas findByCorte', mesas);
    if (!mesas) throw 'No se encontró ningúna mesa.';
    return mesas;
  }
}
