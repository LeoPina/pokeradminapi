import { Entity, Column, OneToMany, JoinColumn, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import { HistorialSaldo } from "../historial-saldo/historial-saldo.entity";
import { Corte } from "../corte/corte.entity";

@Entity({ name: 'Mesa' })
export class Mesa {
    @PrimaryGeneratedColumn()
    IdMesa: number;
    
    @Column({nullable: false})
    IdCorte: number;

    @Column({ nullable: false })
    Fecha: Date;

    @Column({ nullable: false })
    ClaEstatus: number;

    @Column({nullable: false})
    CreditoInicial: number;

    @OneToMany('HistorialSaldo', 'mesas')
    @JoinColumn([
        {
            name: 'IdMesa',
            referencedColumnName: 'IdMesa'
        }
    ])
    saldos: HistorialSaldo[];

    @ManyToOne('Corte', 'mesas')
    @JoinColumn([
        {
            name: 'IdCorte',
            referencedColumnName: 'IdCorte'
        }
    ])
    cortes: Corte;
}