import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';

import { BusinessException } from '../exceptions/business.exception';
import { ValidationException } from '../exceptions/validation.exception';

export interface ValidationResponse {
  mensaje: string;
  control: string;
}

@Injectable()
export class CustomValidationPipe implements PipeTransform {
  async transform(value: any, metadata: ArgumentMetadata) {

    if (value instanceof Object && this.isEmpty(value)) {
      throw new BusinessException(
        'No se recibió información en el request.'
      );
    }
    const { metatype } = metadata;
    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }
    const object = plainToClass(metatype, value);
    const errors = await validate(object);
    if (errors.length > 0) {
      throw new ValidationException(
        this.formatErrors(errors)
      );
    }
    return value;
  }

  private toValidate(metatype): boolean {
    const types = [String, Boolean, Number, Array, Object];
    return !types.find(type => metatype === type);
  }

  private formatErrors(errors: any[]): ValidationResponse[] {
    return errors
      .map(err => <ValidationResponse>{
        mensaje: this.getValidationMessage(err.constraints),
        control: err.property
      })
  }

  private isEmpty(value: any) {
    if (Object.keys(value).length > 0) {
      return false;
    }
    return true;
  }

  private getValidationMessage(constraints: any): string {
    for (let property in constraints) {
      return constraints[property];
    }
  }
}
