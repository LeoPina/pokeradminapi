import { HttpException, HttpStatus } from "@nestjs/common";
import { ValidationResponse } from "../pipes";

export class ValidationException extends HttpException {
    constructor(response: ValidationResponse[]) {
        super(response, HttpStatus.BAD_REQUEST);
    }
}