import { TipoError } from "./tipo-error";
import { ApiProperty } from "@nestjs/swagger";
import { ValidationResponse } from "../pipes";


export class ApiModelException {
    @ApiProperty()
    mensaje: string;
    
    @ApiProperty()
    tipoError: TipoError;

    @ApiProperty()
    Validaciones: ValidationResponse[]

    constructor(mensaje: string, tipoError: TipoError, Validaciones?: ValidationResponse[]) {
        this.mensaje = mensaje;
        this.tipoError = tipoError;
        this.Validaciones = Validaciones;
    }
}