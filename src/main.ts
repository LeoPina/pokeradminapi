import 'reflect-metadata';
import { NestFactory, HttpAdapterHost } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';
import { Seeder } from './db/seed/seeder';
import { CustomValidationPipe } from './shared/pipes';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  .then(context => {
            
    const logger = context.get(Logger);
    const seeder = context.get(Seeder);
    logger.debug('Starting Application...');

    seeder
        .seed()
        .then(() => {
            logger.debug('Seeding phase complete!');

            const { httpAdapter } = context.get(HttpAdapterHost);
            // context.useGlobalFilters(
            //     new GlobalExceptionsFilter(httpAdapter),
            //     new HttpExceptionFilter()
            // );
            context.useGlobalPipes(new CustomValidationPipe());
            // context.useGlobalInterceptors(new TransformInterceptor());
            context.setGlobalPrefix('api');
            // context.use(helmet());
            // context.use(cookieParser());
            // context.use(
            //     rateLimit({
            //         windowMs: 15 * 60 * 1000,
            //         max: 300,
            //     }),
            // );
            context.enableCors();

            const port = process.env.PORT || '30000';
            
            const options = new DocumentBuilder()
                .setTitle('Poker Admin')
                .setBasePath('api')
                .addBearerAuth()
                .setVersion('1.0')
                .build();
            
            const document = SwaggerModule.createDocument(
                context,
                options
            );
            SwaggerModule.setup('swagger', context, document);
            context.listen(port);

            logger.debug(`:rocket:Server running on port ${port}`);
        })
        .catch(error => {
            logger.error('Seeding failed!');
            logger.error(error)
            throw error;
        });
})
.catch(error => {
    console.log('Error!', error);
    
    throw error;
});
  // await app.listen(3000);
}
bootstrap();
