import 'reflect-metadata';
import { Module, Logger } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { CorteModule } from './features/home/home.module';
import { AdministrarModule } from './features/administrar/administrar.module';
import { UsuarioModule } from './features/auth/auth.module';
import { DatosGeneralesModule } from './features/datos-generales/datos-generales.module';
import { DatosGeneralesService } from './features/datos-generales/datos-generales.service';
import { Seeder } from './db/seed/seeder';
import { UsuarioSeederService } from './db/seed/Usuario/usuario-seed.service';
import { SeedModule } from './db/seed/seed.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true, envFilePath: '.env.development' }),

    TypeOrmModule.forRootAsync({
      useFactory: async () => ({
        type: 'mssql' as 'mssql',
        host: process.env.DATABASE_HOST,
        username: process.env.DATABASE_USER,
        password: process.env.DATABASE_PASSWORD,
        database: process.env.DATABASE_NAME,
        schema: process.env.DATABASE_SCHEMA,
        entities: [__dirname + '/**/**/*.entity{.ts,.js}'],
        synchronize: false,
        logging: false,
        migrations: [__dirname + '/db/migrations/**/*{.ts,.js}'],
        cli: {
          migrationsDir: 'src/db/migrations'
        },
        options: {
          encrypt: true,
          packetSize: 81920
        },
        requestTimeout: 300000
      })
    }),
    CorteModule,
    DatosGeneralesModule,
    AdministrarModule,
    UsuarioModule,
    SeedModule
  ],
  providers: [
    DatosGeneralesService,
    Logger,
    Seeder
  ]
})

export class AppModule { }