SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF (OBJECT_ID('[POKSch].[CrearNuevoUsuarioProc]') IS NOT NULL)  DROP PROCEDURE [POKSch].[CrearNuevoUsuarioProc]
GO
CREATE PROCEDURE [POKSch].[CrearNuevoUsuarioProc]
	@Alias		VARCHAR(20),
	@Correo	VARCHAR(20),
	@Contrasena	VARCHAR(20),
    @ImagenPerfil   VARBINARY(MAX)
AS
BEGIN
	
	DECLARE @IdUsuarioMax		INT
	DECLARE @IdSolicitudMax		INT
	
	IF NOT EXISTS(
		SELECT	*
		FROM	POKSCh.Usuario
		WHERE	Correo = @Correo
	)
	BEGIN

		INSERT INTO POKSCh.Usuario 
				(	Alias, Correo,	Contrasena,	ImagenPerfil)
		SELECT		@Alias, @Correo, @Contrasena, @ImagenPerfil


	END
END
GO


